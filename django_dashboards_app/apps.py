from django.apps import AppConfig


class DjangoDashboardConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'django_dashboards_app'
